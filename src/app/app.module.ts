import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'angular2-chartjs';
import { ChartlineComponent } from './chartline/chartline.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [ChartlineComponent],
  imports: [BrowserModule,ChartModule,HttpClientModule],
  entryComponents: [ChartlineComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const customButton = createCustomElement(ChartlineComponent, {
      injector: injector,
     });
     customElements.define('app-chartline', customButton);
  }

  ngDoBootstrap() {}
}
