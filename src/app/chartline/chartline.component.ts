import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-chartline',
  template: `
  <button (click)='getdata()'>Reset</button>
  <div  class='example-loading-shade' *ngIf="isLoadingResults">
  <div *ngIf="isLoadingResults">{{err}}</div>
</div>
  <chart [data]="chartline" [options]="optionline"> </chart>
  `,
  styles: [
    `
    .example-loading-shade {
      position: absolute;
      top: 0;
      left: 0;
      bottom:0;
      right: 0;
      background: rgba(0, 0, 0, 0.15);
      z-index: 1;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    `
  ],
  encapsulation: ViewEncapsulation.Native
})
export class ChartlineComponent implements OnInit{

  

constructor(private http:HttpService) { }
  optionline:any;
  chartline: any;
  length:any;
  datachartline: number[];
  namechartline: any;
  labelschartline: any;
  datalocalStorage=[];
  type:string;
  isLoadingResults = true;
   @Input() url='';
   err:string;
  color=['#008080','#000000','#007bff','#6610f2','#e83e8c','#dc3545','#fd7e14','#ffc107','#28a745']
    ngOnInit(): void {
      this.getdata() 

  }

 
  getdata() {
    this.http.getchartline(this.url)
    .subscribe(
      (res)=>{
        
      var getdata=[]
      var getname=[]
      var getlabel=res[0].label;
      this.type=res[0].type;
      for(let i=0;i<res.length;i++){
       getdata.push(res[i].data)
       getname.push(res[i].name)
      }
      this.datachartline=getdata;
      this.namechartline=getname;
      this.labelschartline=getlabel;
      this.length=res.length;
      console.log(this.datachartline)
      this.isLoadingResults = false;
      },
      (err)=>{
        this.isLoadingResults = true;
       this.err=err;
      },
       ()=>{
        this.Chartline();
        this.isLoadingResults = false;
    }
    )
  }

  Chartline() {
    this.chartline = {
      labels: 
        this.labelschartline
      ,
      datasets: [

      ]
    };
    for(let i=0;i<this.length;i++){
      this.chartline.datasets.push({
        type:this.type,
          label: this.namechartline[i],
          data: this.datachartline[i],
          borderColor: this.color[i],
          fill: false,
          lineTension: 0.1,
          borderWidth: 1
          
      })
  }

  this.optionline = {
    responsive: true,
    pointHoverRadius: 0
    ,
     maintainAspectRatio: true,
    elements: {
    
    },
   
    scales: {
      xAxes: [{
        display: true
      }],
      yAxes: [{
        display: true
      }],
    },
    pointHoverBorderWidth: false

  };
  }

}
